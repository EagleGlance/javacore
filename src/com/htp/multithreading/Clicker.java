package com.htp.multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class Clicker extends Thread {
    private int click = 0;
    private volatile boolean stopFlag = false;

    public void run(){
        while(!stopFlag) {
            click++;
            System.out.println(click);
        }
    }

    public void changeStopFlag() {
        stopFlag = true;
    }

    public int getClick() {
        return click;
    }
}
