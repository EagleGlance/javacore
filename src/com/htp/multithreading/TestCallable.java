package com.htp.multithreading;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TestCallable implements Callable<Integer> {
    private int delay;
    private final Lock lock = new ReentrantLock();
    private AtomicInteger ai = new AtomicInteger();

    public TestCallable(int delay) {
        this.delay = delay;
    }

    public void someLogic() {
        ai.incrementAndGet();
    }

    @Override
    public Integer call() throws Exception {
        Thread.sleep(1000 * delay);
        return delay*65;
    }
}
