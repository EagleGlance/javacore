package com.htp.multithreading;

public class TreadChild extends Thread {

    private static Object lock = new Object(); //lock - monitor - false

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
            /*Write info into file*/
            System.out.println(i);
        }
    }

//    public static synchronized void doSomething() {
//
//    }
//
//    public void printSomething(){
//        synchronized (lock) { //critical section
//
//        }
//    }



}
