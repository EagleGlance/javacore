package com.htp.multithreading;

public class RunnableChild implements Runnable {
    @Override
    public void run() {
        System.out.println("Runnable implementation of run method!");
    }
}
