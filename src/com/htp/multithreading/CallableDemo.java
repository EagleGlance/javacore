package com.htp.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class CallableDemo {
    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Integer> thirdResult = executorService.submit(new TestCallable(10));
        Future<Integer> firstResult = executorService.submit(new TestCallable(2));
        Future<Integer> secondResult = executorService.submit(new TestCallable(5));

        System.out.println(firstResult.isDone());
        System.out.println(firstResult.get());

        System.out.println(secondResult.isDone());
        System.out.println(secondResult.get());

        System.out.println(thirdResult.isDone());
        System.out.println(thirdResult.get());

        executorService.shutdown();

    }
}
