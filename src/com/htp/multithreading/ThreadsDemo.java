package com.htp.multithreading;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


class ThreadsDemo {
    public static void main(String[] args) throws InterruptedException, IOException {

        Clicker clicker = new Clicker();
        clicker.start();
        clicker.sleep(30);

        clicker.changeStopFlag();
        clicker.join();

//        Files.lines(Paths.get("F:\\Торрент\\Java\\parser.txt"))
//                .filter(s -> s.length() < 8) //промежуточные операции
//                .map(String::toUpperCase)
//                .peek((e) -> System.out.print("," + e))
//                .collect(Collectors.toList()); //терминальная операция
//
//
//        String stringStream = "HTP Java";
//        stringStream.chars();
//
//        Stream.builder().add("a1").add("a2").add("a3").build();
//
//        Arrays.asList("q", "w", "e").parallelStream();

//        List<String> listForStreams = Arrays.asList("a1", "a2", "a2", "a3");
//
//        String collect = listForStreams.stream().distinct().collect(Collectors.joining(","));
//        listForStreams.stream().filter(s -> s.length() > 10).distinct().collect(Collectors.toList());
//        listForStreams.stream().map(s -> s.toUpperCase()).distinct().collect(Collectors.toList());
//        new HashMap<>().entrySet().stream();


        //1. Получить уникальные строки
        //2. Вывести строки длиной больше 10
        //3. Вывести получаемую строку в upperCAse and LowerCase
        //4. Дан массив int[] array = new int[]{1, 2, 3, 4, 5, 6, 2, 3}
        //подсчитать количество вхождений каждого элемента массива и вывести элемент с максимальным вхождением
//
//        List<Integer> lst = new ArrayList<>();
//
//        lst.add(1);
//        lst.add(2);
//        lst.add(3);
//        lst.add(4);
//        lst.add(5);
//        lst.add(6);
//        lst.add(2);
//        lst.add(3);
//
//        lst.forEach(System.out::println); //method reference
//        lst.stream().peek(System.out::println).collect(Collectors.toList()); //method reference
//        lst.forEach(e -> System.out.println(e)); //method reference
//        lst.stream().reduce(Integer::sum); //method reference
//        lst.stream().reduce((s1, s2) -> s1 + s2); //method reference
//        lst.stream().reduce((s1, s2) -> s1 - s2); //method reference
//
//        lst.stream().collect(Collectors.summingInt(((p) -> p % 2 == 1 ? p : 0)));
//
//        Map<Integer, Integer> duplic = new HashMap<Integer, Integer>();
//        for (Integer string : lst) {
//            if (duplic.keySet().contains(string)) {
//                duplic.put(string, duplic.get(string) + 1);
//            } else {
//                duplic.put(string, 1);
//            }
//        }
//        System.out.println("Duplicate: " + duplic);
//        System.out.println();

    }

}
//
//        int[] someArray = new int[]{1, 2, 3};
//
//        List<Integer> objects = new ArrayList<>(); //[1, 2, 3, 4]
//        objects.add(1);
//        objects.add(2);
//        objects.add(3);
//        objects.add(4);
//
//        List<A> testList = new ArrayList<>();
//        testList.add(new A());
//        testList.add(new A());
//        testList.add(new A());
//
//        List<Integer> collect1 = objects.stream()
//                .filter(e -> e > 2)
//                .map(e -> e * 2)
//                .collect(Collectors.toList());
//
//        Arrays.asList("a1", "a2", "a3");
//
//
//        Collection<String> collection = Arrays.asList("a1", "a2", "a3");
//        Stream<String> streamFromCollection = collection.stream();
//
//        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4);
//        Arrays.stream(someArray);
//
//
//        List<Integer> collect = objects.stream()
//                .filter(e -> e % 2 != 0) //промежуточные операторы
//                .map(e -> e * 100)
//                .map(element -> {
//                    int i = 0;
//                    i++;
//                    return element * 100 * i;
//                })
//                .map(element -> element * 100)
//                .collect(Collectors.toList()); //терминальный оператор
//
//        Set<Integer> collectSet = objects.stream()
//                .filter(e -> e % 2 != 0) //промежуточные операторы
//                .map(e -> e * 100)
//                .collect(Collectors.toSet()); //терминальный оператор
//
//
//
//        for (Integer object : objects) {
//            System.out.println(object);
//        }
//
//        for (Integer integer : collect) {
//            System.out.println(integer);
//        }
//
//
//    }

