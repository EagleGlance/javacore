package com.htp;

import java.util.ArrayList;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        char c = ':';

        int firstParam = 2;
        System.out.println(firstParam++); //++ = + 1
        //System.out.println(firstParam = firstParam + 1); //++ = + 1
        //System.out.println(firstParam += 1); //++ = + 1
        System.out.println(++firstParam);
        firstParam *= 2;
        //firstParam = firstParam * 2;
        System.out.println(firstParam);

        firstParam /= 2;
        System.out.println(firstParam);

        int result = 5 % 2;
        System.out.println(result);

        if (!false) { //&& = and, || = or
            System.out.println("qweqweqwe");
        }

        if (5 % 2 == 0) {
            System.out.println("qweqweq");
        } else {
            System.out.println("zzzzzz");
        }

        System.out.println(5 % 2 == 0 ? "qweqweq" : "zzzzzz");
        System.out.println(5 % 2 == 0); //ctrl+d
        System.out.println(5 % 2 == 1);
        System.out.println(5 % 2 != 10);
    }
}
