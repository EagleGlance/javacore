package com.htp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
        import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectionsDemo {
    public static void main(String[] args) {
        List<String> objects = new ArrayList<>();
        objects.add("123");
        objects.add("456");
        objects.add("789");

        List<Integer> integerList = new ArrayList<>();
        integerList.add(777);

        List<Object> mix = new ArrayList<>();
        mix.addAll(objects);
        mix.addAll(integerList);

        System.out.println("String Collection");
        System.out.println("String Collection Size:" + objects.size());
        System.out.println("String Collection [0]:" + objects.get(0));
        System.out.println("String Collection Contains test:" + objects.contains("789"));
        System.out.println("String Collection Contains test:" + objects.contains("qweqwe"));
        for (String object : objects) {
            System.out.println(object);
        }

        System.out.println("Integer Collection");
        for (Integer integer : integerList) {
            System.out.println(integer);
        }

        System.out.println("Mix Collection");
        for (Object o : mix) {
            System.out.println(o);
        }

        LinkedList<Object> linkedList = new LinkedList<>();
        linkedList.add("First");
        linkedList.add("Second");
        linkedList.add("Third");

        linkedList.add(1, "Insert");

        System.out.println("Linked List Test");
        Iterator<Object> iterator = linkedList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        Map<String, Object> demoMap = new LinkedHashMap<>();
        demoMap.put("key", "value");
        demoMap.put("key1", "value1");
        demoMap.put("key2", "value2");

        System.out.println("Map Demo");
        for (Map.Entry<String, Object> entry : demoMap.entrySet()) {
            System.out.print("Entry Key: " + entry.getKey());
            System.out.println(" | Entry Value: " + entry.getValue());
        }

        Set<String> hashSet = new LinkedHashSet<>();
        hashSet.add("");
        hashSet.add("qwe");
        hashSet.add("qwe");
        hashSet.add("ZZZ");
        hashSet.add("ZZZ");

        for (String s : hashSet) {
            System.out.println(s);
        }
    }
}
