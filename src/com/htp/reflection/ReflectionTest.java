package com.htp.reflection;

import com.htp.polymorph.A;

@MyAnnotation("Class Annotation")
public class ReflectionTest implements Cloneable, ReflectionInterface {

    @MyAnnotation("Field Annotation")
    private String testString;
    private Long testLong;
    private A testA;

    public ReflectionTest() {
    }

    private ReflectionTest(String testString, Long testLong) {
        this.testString = testString;
        this.testLong = testLong;
        this.testA = new A();
    }

    public ReflectionTest(String testString, Long testLong, A testA) {
        this.testString = testString;
        this.testLong = testLong;
        this.testA = testA;
    }

    public void method() {
        System.out.println("Method invocation");
    }

    @MyAnnotation("Method Annotation")
    private void method(Long first, Long second) {
        System.out.println("Method invocation " + first + " " + second);
    }

    private void method1() {
        System.out.println("Method1 invocation ");
    }

    @MyAnnotation("Method Annotation")
    @Override
    public String toString() {
        return testString + "   " + testLong;
    }
}
