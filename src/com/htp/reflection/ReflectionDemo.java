package com.htp.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ReflectionDemo {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        ReflectionTest reflectionTest = new ReflectionTest();
        Class<? extends ReflectionTest> aClass = reflectionTest.getClass();
        System.out.println(aClass);
        System.out.println(Integer.class);
        System.out.println(Integer.class.getSimpleName());
        System.out.println(Integer.class.getCanonicalName());
        System.out.println(int.class);

        int modifiers = aClass.getModifiers();
        System.out.println(modifiers);

        if (Modifier.isPublic(modifiers)) {
            System.out.println("public");
        }
        if (Modifier.isAbstract(modifiers)) {
            System.out.println("abstract");
        }
        if (Modifier.isFinal(modifiers)) {
            System.out.println("final");
        }

        System.out.println(aClass.getSuperclass());
        System.out.println(ReflectionTestChild.class.getSuperclass());

        System.out.println(Arrays.stream(aClass.getInterfaces()).map(Class::getName).collect(Collectors.joining(", ")));

        System.out.println("******************************");
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            System.out.println(Modifier.toString(field.getModifiers()));
            System.out.println(field.getType());
            System.out.println(field.getName());
        }

        String fieldName = "testString";
        try {
            System.out.println(aClass.getField(fieldName));
            System.out.println(aClass.getDeclaredField(fieldName));
        } catch (NoSuchFieldException e) {
            System.err.println("Field with name " + fieldName + " was not found!");
        }

        System.out.println("*****Constructors info******");
        Constructor[] constructors = aClass.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            Class[] paramTypes = constructor.getParameterTypes();
            System.out.print(constructor.isSynthetic() + " ");
            System.out.print(Modifier.toString(constructor.getModifiers()) + " ");

            for (Class paramType : paramTypes) {
                System.out.print(paramType.getName() + " ");
            }
            System.out.println();
        }

        Class[] params = {String.class, Long.class};
        ReflectionTest testObject = null;
        try {
            Constructor<? extends ReflectionTest> declaredConstructor = aClass.getDeclaredConstructor(params);
            declaredConstructor.setAccessible(true);
            testObject = declaredConstructor.newInstance("Test", 1000L);
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        System.out.println(testObject);

        System.out.println("********Method test**********");
        Class[] methodParams = {Long.class, Long.class};
        try {
            Method method = aClass.getDeclaredMethod("method", methodParams);
            method.setAccessible(true);
            method.invoke(testObject, 10L, 20L);
        } catch (InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        System.out.println("***********Annotations info *******");
        Annotation[] annotations = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }

        Method[] declaredMethods = aClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            declaredMethod.setAccessible(true);
            if (declaredMethod.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation annotation = declaredMethod.getAnnotation(MyAnnotation.class);
                System.out.println(annotation.value());
                System.out.println("Yahoo! I am marked " + declaredMethod.getName());
            }
        }
    }
}
