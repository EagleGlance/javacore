//possible to write comment
package com.htp.lesson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HTP {
    public static final String LOCALHOST;
    public static int counter = 0;

    static {
        counter = 100;
        LOCALHOST = "127.0.0.1";
        System.out.println("In 1 static block");
    }

    static {
        counter = 200;
        System.out.println("In 2 static block");
    }

    static {
        counter = 500;
        System.out.println("In 3 static block");
    }

    private final Long constant = 1L;
    private Long id;
    private String name;
    private String surname;
    private Long age;
    private ClassForField test;

    {
        id = 1L;
        age = 10L;
        System.out.println("In age logic block");
    }

    {
        surname = "Surname";
        System.out.println("In surname static block");
    }

    {
        name = "Name";
        System.out.println("In name static block");
    }

    public HTP() {
        System.out.println("In default constructor");
    }

    public HTP(Long id, String name, String surname, Long age, ClassForField test) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.test = test;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public void someMethod() {
        int i = 0;
        long l = 0;
        boolean b = true;

        Integer ii = new Integer(i);
        Integer iii = i; //упаковки

        int test = iii.intValue(); //автораспоковка

        List<Integer> qwe = new ArrayList<>();
        qwe.add(ii);
    }

    public ClassForField getTest() {
        return test;
    }

    public void setTest(ClassForField test) {
        this.test = test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HTP htp = (HTP) o;
        return Objects.equals(constant, htp.constant) &&
                Objects.equals(id, htp.id) &&
                Objects.equals(name, htp.name) &&
                Objects.equals(surname, htp.surname) &&
                Objects.equals(age, htp.age) &&
                Objects.equals(test, htp.test);
    }

    @Override
    public int hashCode() {
        return Objects.hash(constant, id, name, surname, age, test);
    }

    @Override
    public String toString() {
        return "HTP{" +
                "constant=" + constant +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", test=" + test +
                '}';
    }
}
