package com.htp.resourse;

import java.util.Locale;

public class ResourceBundleDemo {
    public static void main(String[] args) {
        //PageReader instance = PageReader.getInstance(Locale.US);
        PageReader instance = PageReader.getInstance(Locale.FRANCE);

        System.out.println(instance.getProperty(PageReader.PAGE_INFO_PROPERTY));
    }
}
