package com.htp.resourse;

import java.util.Locale;
import java.util.ResourceBundle;

public class PageReader {
    static PageReader instance;
    private ResourceBundle resourceBundle;

    private static final String BUNDLE_NAME = "page";
    public static final String PAGE_INFO_PROPERTY = "page.info";

    public static PageReader getInstance(Locale locale) {
        if (instance == null) {
            instance = new PageReader();
            //Read page.properties file from resources package and save info from file to resourceBundle variable
            instance.resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
        }
        return instance;
    }

    public String getProperty(String key) {
        return (String)resourceBundle.getObject(key);
    }
}
