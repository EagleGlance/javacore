package com.htp.oop;

public class GenericInterfaceImplementor implements GenericInterface<Long, Cat> {
    @Override
    public Long someMethod() {
        return null;
    }

    @Override
    public Long someMethod(Cat value) {
        return null;
    }

    @Override
    public int simpleMethod() {
        return 0;
    }
}
