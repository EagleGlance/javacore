package com.htp.oop;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CollectionsDemo {
    public static void main(String[] args) {
        HashSet<Cat> cats = new HashSet<>();
        LinkedHashSet<Cat> linkedCats = new LinkedHashSet<>();
        Set<Cat> catTreeSet = new TreeSet<>();

        Map<String, Cat> mapOfCats = new HashMap<>();

        Cat cat1 = new Cat("kotic", 10, 9, Sex.MALE, "Slavyan", "domashnii");
        Cat cat2 = new Cat("kotic", 11, 9, Sex.MALE, "Slavyan", "domashnii");
        Cat cat3 = new Cat("kotic", 12, 9, Sex.MALE, "Slavyan", "domashnii");
        Cat cat4 = new Cat("kotic", 14, 9, Sex.MALE, "Slavyan", "domashnii");
        Cat cat5 = new Cat("kotic", 18, 9, Sex.MALE, "Slavyan", "domashnii");

        mapOfCats.put("cat1", cat1);
        mapOfCats.put("cat1", cat2);
        mapOfCats.put("cat3", cat3);
        mapOfCats.put(null, cat4);
        mapOfCats.put(null, cat5);

        System.out.println("Hash Map testing");
        System.out.println("---------------------------------");
        System.out.println(mapOfCats.get("cat"));
        System.out.println(mapOfCats.get("cat1"));
        //System.out.println(mapOfCats.get(null));
        System.out.println("---------------------------------");

        Set<Map.Entry<String, Cat>> entries = mapOfCats.entrySet();
        for (Map.Entry<String, Cat> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
        System.out.println("---------------------------------");

        cats.add(cat1);
        cats.add(cat2);
        cats.add(cat3);
        cats.add(cat4);
        cats.add(cat5);

        linkedCats.add(cat1);
        linkedCats.add(cat2);
        linkedCats.add(cat3);
        linkedCats.add(cat4);
        linkedCats.add(cat5);

        catTreeSet.add(cat1);
        catTreeSet.add(cat2);
        catTreeSet.add(cat3);
        catTreeSet.add(cat4);
        catTreeSet.add(cat5);

        System.out.println("-------------------Simple--------------");
        for (Cat cat : cats) {
            System.out.println(cat);
        }
        System.out.println("---------------------------------");

        System.out.println("------------------Linked---------------");
        for (Cat linkedCat : linkedCats) {
            System.out.println(linkedCat);
        }
        System.out.println("---------------------------------");

        System.out.println("------------------Tree---------------");
        for (Cat treeCat : catTreeSet) {
            System.out.println(treeCat);
        }
        System.out.println("---------------------------------");

    }
}
