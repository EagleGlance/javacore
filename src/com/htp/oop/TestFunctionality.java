package com.htp.oop;

public interface TestFunctionality {
    int TEST_INTERFACE_VALUE = 10;

    default String commentFunctionality() {
        return "";
    }
}
