package com.htp.oop;

public interface ShipFunctionalityInterface {
    /*This method will be provide functionality for ship fights
    * return*/
    String fight();

    String move();

    default String commentFunctionality1() {
       return "";
    }
}
