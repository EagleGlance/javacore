package com.htp.oop;

public class GenericClass<T1 extends Number, T2 extends Number> {
    private Integer value;
    private T1 templateValue;
    private T2 templateValue2;
    //private static T2 qwe; compilation error

    public GenericClass() {
    }

    public GenericClass(Integer value, T1 templateValue, T2 templateValue2) {
        this.value = value;
        this.templateValue = templateValue;
        this.templateValue2 = templateValue2;
    }

    public static void doSomething(){
        System.out.println("123123");
    }

    public void someMethod(T1 someParam, T2 anotherParam) {
    }
}
