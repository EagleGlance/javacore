package com.htp.oop;

public class Dog extends Animal {
    public Dog(String animalName, double weight, int liveCount, Sex gender) {
        super(animalName, weight, liveCount, gender);
    }

    @Override
    public void somethingDo() {
        System.out.println("Do, dog!");
    }
}
