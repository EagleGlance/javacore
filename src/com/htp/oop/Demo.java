package com.htp.oop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class Demo {
    public static void main(String[] args) {

        Integer integer = new Integer(10);
        Long aLong = new Long(777);
        Long aLong1 = 777L; //-> new Long(777);

        int testInt = integer; //integer.intValue();

        System.out.println(aLong);
        System.out.println(integer);

        char[] testArray = {'a', 'b', 'c'};
        byte[] byteArray = {66, 77, 88};
        int a = 'c';
        System.out.println("Char test: " + a);
        System.out.println(new String(testArray));
        System.out.println(new String(byteArray));
        System.out.println("asdasdasd");
        System.out.println(new String("asdasdasd" + "q2123123").intern());
        System.out.println("asdasdasd".equals("qweqweqw"));
        System.out.println("     asdasdasd      ".equalsIgnoreCase("     ASDASDASD      "));
        System.out.println("asdasdasd".concat("qweqweqw"));
        System.out.println("asdasdasd".length());
        System.out.println("asdasdasd".contains("sd"));
        System.out.println("asdasdasd".contains("qw"));
        System.out.println("asdasdasd".indexOf("qw"));
        System.out.println("asdasdasd".replace("as", "QQQ"));
        System.out.println("     asdasdasd      ".trim());
        System.out.println("     asdasdasd      ".substring(1));
        System.out.println("     asdasdasd      ".toLowerCase());
        System.out.println("     asdasdasd      ".toUpperCase());
        System.out.println("asdasdasd".split(" "));

        String[] words = "asdasdasd".split(" ");

        char[] symbols = {',', '/', '*', '?'};

        for (String word : words) {

            System.out.println(word.toUpperCase());
        }

        int[] zzz = {};
        int[] xxx = new int[3];
        int[] xxxx = new int[]{1, 2};

//        //Animal animal = new Animal();
////
////        testAnimal.run();
////        cat.run();
////        System.out.println(TestFunctionality.TEST_INTERFACE_VALUE);
////
////        TestClass testClass1 = new TestClass();
////        TestFunctionality testClass = new TestClass();
////        ShipFunctionalityInterface shipFunctionalityInterface = new TestClass();
////        testClass1.commentFunctionality();
////        testClass1.move();
////        testClass1.getTestInt();
////
////        testClass.commentFunctionality();
////        shipFunctionalityInterface.move();
//
//        List<Cat> cats = new ArrayList<Cat>();
//        for (int i = 0; i < 10; i++) {
//            cats.add(new Cat());
//        }
        Cat cat1 = new Cat() {
            public void someMethod() {
                System.out.println("Test New Method");
            }

            @Override
            public String getCatType() {
                setWeight(12000);
                someMethod();
                return "Overrided method";
            }
        };

        GenericClass<Integer, Integer> qweqwe
                = new GenericClass<>(1, 2, 2);

        GenericClass<Integer, Integer> qweqwe1
                = new GenericClass<>(1, 2, 2);

        new GenericChild(1, 2L, 4L);

        Cat cat = new Cat();

        System.out.println("Simple: " + cat.getCatType());
        System.out.println("Anonymous" + cat1.getCatType());

        List<Integer> objects = new ArrayList<>(10);
        objects.add(123);
        objects.add(456);
        objects.add(2, 9);
        System.out.println(objects.size());
        System.out.println(objects.contains(123));
        System.out.println(objects.contains(777));
        Object[] objects1 = objects.toArray();

        Iterator<Integer> iterator = objects.iterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        for (Integer object : objects) {
            System.out.println(object);
        }

        Arrays.toString(objects.toArray());

        //apache commons-lang-3.jar


//        System.out.println(Sex.NOT_SELECTED);
//        System.out.println(Sex.FEMALE);
//        System.out.println(Sex.MALE);
//
//        for (Cat cat : cats) {
//            System.out.println(cat);
//        }

//        Dog dog = new Dog();
//        dog.somethingDo();
    }
}
