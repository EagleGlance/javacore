package com.htp.oop;

public interface GenericInterface<K, V> {
    K someMethod();
    K someMethod(V value);

    int simpleMethod();
}
