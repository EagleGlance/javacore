package com.htp.oop;

import com.htp.polymorph.A;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

public class Cat extends Animal implements Comparator<Cat>, Comparable<Cat>, Serializable {

    private static final long serialVersionUID = 2L;

    private static int info = 0;
    private transient int trash = 0;

    private String ownerName;
    private String catType;

    public <T> void someMethod(T param){

    }

    public Cat() {
        super("Test Cat", 5, 9, Sex.NOT_SELECTED);
        System.out.println("In Cat default constructor");
    }

    public Cat(String animalName, double weight, int liveCount, Sex gender, String ownerName, String catType) {
        super(animalName, weight, liveCount, gender);
        this.ownerName = ownerName;
        this.catType = catType;
        System.out.println("In Cat params constructor");
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * @return
     */
    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    @Override
    public int compare(Cat o1, Cat o2) {
        if (o1.getWeight() > o2.getWeight()) {
            return -1;
        } else {
            if (o1.getWeight() < o2.getWeight()) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    @Override
public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Cat cat = (Cat) o;
        return Objects.equals(ownerName, cat.ownerName) &&
                Objects.equals(catType, cat.catType);
    }

    @Override
    public int hashCode() {
return Objects.hash(super.hashCode(), ownerName, catType);
    }

    @Override
    public String toString() {
        return super.toString() + "ownerName :" + ownerName;
    }

    @Override
    public void run() {
        System.out.println("Run, cat, run!");
    }

    @Override
    public void somethingDo() {
        System.out.println("Do, cat!");
    }

    public long run(long someDistance) {
        System.out.println("Run, boy, run! " + someDistance);
        return someDistance;
    }

    @Override
    public int compareTo(Cat o) {
        if (this.getWeight() > o.getWeight()) {
            return 1;
        } else {
            if (this.getWeight() < o.getWeight()) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
