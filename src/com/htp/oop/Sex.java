package com.htp.oop;

public enum Sex implements TestFunctionality {
    FEMALE("Female", "Female Gender"),
    MALE("Male", "Male Gender"),
    NOT_SELECTED("Not Selected", "Not Selected");

    public String genderInfo;
    public String description;

    Sex(String genderInfo, String description) {
        this.genderInfo = genderInfo;
        this.description = description;
    }

    public String getGenderInfo() {
        return genderInfo;
    }

    public String getDescription() {
        return description;
    }


    @Override
    public String toString() {
        return this.description + " " +  this.genderInfo + " " + this.name() + " " + this.ordinal();
    }
}