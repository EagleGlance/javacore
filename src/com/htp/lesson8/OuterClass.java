package com.htp.lesson8;

import com.htp.polymorph.MyFunctionaInterface;

import java.util.Date;

public class OuterClass {
    private static int constanta = 10;
    private Inner inner;
    private String tempStr;
    private Date date;

    public OuterClass() {
        tempStr = "Some outer string";
        date = new Date();
        inner = new Inner();
    }

    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        outerClass.smthMethod();
        Inner inner = new OuterClass().new Inner(10, "Hello inner class!");
        System.out.println(inner.getInnerInt() + " : " + inner.getInnerString() + inner.innerString);
        System.out.println(OuterClass.Inner.FINAL_INNER_INT);
    }

    public void smthMethod() {
        System.out.println(inner.innerInt);
        System.out.println(inner.innerString);

        // local class
        class InnerMethodClass {
            private String innerMethodStr;

            public InnerMethodClass(String innerMethodStr) {
                this.innerMethodStr = innerMethodStr;
            }

            public String getInnerMethodStr() {
                return innerMethodStr;
            }

            public void setInnerMethodStr(String innerMethodStr) {
                this.innerMethodStr = innerMethodStr;
            }
        }

        System.out.println(new InnerMethodClass("ZachemNamTakoe!").getInnerMethodStr());
    }

    class Inner implements MyFunctionaInterface {
        public static final int FINAL_INNER_INT = 100_000_000;
        private int innerInt = 0;
        private String innerString = "innerString";
        //public static int staticInnerInt;

        public Inner() {
        }

        public Inner(int innerInt, String innerString) {
            this.innerInt = innerInt;
            this.innerString = innerString;
        }

        public int getInnerInt() {
            return innerInt;
        }

        public void setInnerInt(int innerInt) {
            this.innerInt = innerInt;
        }

        public String getInnerString() {
            return innerString;
        }

        public void setInnerString(String innerString) {
            this.innerString = innerString;
        }

        public void method() {
            System.out.println(tempStr);
            System.out.println(date);
        }

        public void smthMethod() {

        }

        @Override
        public void doSomething() {

        }
    }

    class ChildInner extends Inner {

    }
}
