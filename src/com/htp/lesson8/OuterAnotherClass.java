package com.htp.lesson8;

import java.util.Date;

public class OuterAnotherClass {
    private String outerString;
    private Date outerDate;
    private static int counter = 0;

    public final int constanta = 10;

    public OuterAnotherClass() {
    }

    public OuterAnotherClass(String outerString, Date outerDate) {
        this.outerString = outerString;
        this.outerDate = outerDate;
    }

    public String getOuterString() {
        return outerString;
    }

    public void setOuterString(String outerString) {
        this.outerString = outerString;
    }

    public Date getOuterDate() {
        return outerDate;
    }

    public void setOuterDate(Date outerDate) {
        this.outerDate = outerDate;
    }

    public int getConstanta() {
        return constanta;
    }

    static class StaticClass {
        public static void someMethod() {
            OuterAnotherClass outerAnotherClass = new OuterAnotherClass();
            //System.out.println(outerDate);
            System.out.println(counter);
            System.out.println("123123123");
        }
    }

    public static void main(String[] args) {
        OuterAnotherClass.StaticClass.someMethod();
    }
}
