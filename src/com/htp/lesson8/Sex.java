package com.htp.lesson8;

public enum Sex {
    MALE(100), FEMALE(2), NOT_SELECTED(3);

    private int qwe;

    Sex(int qwe) {
        this.qwe = qwe;
    }

    public void setQwe(int qwe) {
        this.qwe = qwe;
    }

    public int getQwe() {
        return qwe;
    }
}
