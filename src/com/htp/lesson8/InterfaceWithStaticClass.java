package com.htp.lesson8;

import com.htp.oop.TestFunctionality;

public interface InterfaceWithStaticClass {
    int x = 10;

    public final static int i = 10;

    /*Deprecated*/
    class InnerClass implements TestFunctionality {
//        @Override
//        public String commentFunctionality() {
//            return "Zacheeem";
//        }
    }

    default void pringSmth() {

    }
}
