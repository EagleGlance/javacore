package com.htp.lesson8;

public class Lesson8 {
  private String testCharArray;

  public Lesson8(String testCharArray) {
    this.testCharArray = testCharArray;
  }

  public static void main(String[] args) {
    new Lesson8("123").testStringPoolLiterals();
  }

  //F9  - переход к следующему бреуйкпоинту(продолжение работы приложения)
  //F8 - переход на следующую строку кода

  public String createString(char[] charArray) {
    return new String(charArray);
  }

  public String createString(byte[] byteArray) {
    return new String(byteArray);
  }

  public String getTestCharArray() {
    return testCharArray;
  }

  public void setTestCharArray(String testCharArray) {
    this.testCharArray = testCharArray;
  }

  public void testStringPoolLiterals() {

    System.out.println("Ку Ку Петя".replaceAll("Петя", "ВоВа"));

    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append(123).append("asdasd\n").append('a').append(" " + new Object());
    byte b = 127;
    byte b1 = 10;
    byte result = ++b;
    System.out.println(result);

    System.out.println(stringBuilder.toString());

    System.out.println("Ку Ку Петя".lastIndexOf("у"));

    Sex.FEMALE.setQwe(4);

    System.out.println(Sex.FEMALE.getQwe());

    // Сгенерировать строку доино 100 симовлов
    // Найти последнее вхождение буквы Z в этой строке и вывести ее идекс
    // Найти паттерн xx в этой  строке и заменить на большие буквы QQ

    //
    //    System.out.println(String.format("Test %.8f string format %s", 123F, 123));
  }
}
