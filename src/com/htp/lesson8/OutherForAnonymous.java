package com.htp.lesson8;

public class OutherForAnonymous {
  private String qwe = "test";

  public OutherForAnonymous() {}

  public OutherForAnonymous(String qwe) {
    this.qwe = qwe;
  }

  public String getQwe() {
    return qwe;
  }

  public void setQwe(String qwe) {
    this.qwe = qwe;
  }

  public void methodForAnonymous() {
    System.out.println("Hello from OuterClass");
  }
}
