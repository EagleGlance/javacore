package com.htp.polymorph;

public interface SecondInterface extends FirstInterface {
    String HTP_STRING_CONSTANT = "HTP";

    void methodInSecondInterface();

    void doingSOmethingNEw();
}
