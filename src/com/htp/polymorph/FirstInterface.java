package com.htp.polymorph;

public interface FirstInterface {
    String LOCALHOST = "";
    String LOCALHOST_US = "";

    void printSomething();
    int qweqwe();

    default void print() {

    }

    public static int qwe() {
        return 0;
    }
}
