package com.htp.polymorph;

import java.io.Serializable;

public class A extends ParentAbstract implements Serializable, Cloneable, SecondInterface, ThirdInterface {

    public A() {

    }

    @Override
    public void somethingDoing() {

    }

    public A(int a) {

    }

    public A(int a, int b) {

    }

    public void method() {
        System.out.println("Some method in A");
    }

    public void method(int a) {
        System.out.println("Some method with param in A");
    }

    public void method(int a, int b) {
        System.out.println("Some method with params in A");
    }

    public void method(int a, int b, int c) {

    }

    @Override
    public void methodInSecondInterface() {

    }

    @Override
    public void doingSOmethingNEw() {

    }

    @Override
    public void printSomething() {

    }

    @Override
    public int qweqwe() {
        return 0;
    }

    @Override
    public void printInfoFrom() {

    }
}
