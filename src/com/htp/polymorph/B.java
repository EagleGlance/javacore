package com.htp.polymorph;

public class B extends A {

    @Override
    public void method() {
        super.method();
        System.out.println("Some method in B");
    }

    public void method(int a, int b) {
        System.out.println("Some method with params in B");
    }

    public void method(int a, int b, int c, int d) {

    }

    @Override
    public void somethingDoing() {

    }

    @Override
    public String toString() {
        return "" + "" +

                new StringBuffer()
                .append("qweqweqwe")
                .append(123142)
                .append('c')
                .toString();
    }
}
