package com.htp.polymorph;

import com.htp.polymorph.enums.Gender;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DemoPolymorph {
    public static void main(String[] args) {
//
//        A a = new A();
//        FirstInterface q = new A();
//
//        if (a instanceof B) {
//            System.out.println("I am implementor of Interface");
//        }
//
//        Map<String, Object> someMap = new HashMap<>();
//
//        for (Map.Entry<String, Object> stringObjectEntry : someMap.entrySet()) {
//            System.out.println(stringObjectEntry.getKey());
//        }
//
//        A aa = new B();
//        B b = new B();
//        a.method();
//        aa.method();
//        b.method();

//        Object[] trash = new Object[] {
//                new User(), new A(), new B()
//        };
//
//        for (Object o : trash) {
//            if (o instanceof User) {
//                System.out.println("User : " + (User) o);
//            }
//
//            if (o instanceof B) {
//                System.out.println("B : " + (B) o);
//            }
//
//            if (o instanceof A) {
//                System.out.println("A : " + (A) o);
//            }
//        }
//
//        String s1 = new String("Hello").intern();
//        String s2 = new StringBuffer("He").append("llo").toString(); // "Hel" + "lo"
//        String s3 = s2.intern();
//        System.out.println("s1 == s2? " + (s1 == s2));
//        System.out.println("s1 == s3? " + (s1 == s3));
//
//        String hello = "Hello";
//        String lo = "lo";
//        System.out.print((hello == "Hello") + " ");
//
//        System.out.print((hello == ("Hel" + "lo")) + " ");
//        System.out.print((hello == ("Hel" + lo)) + " "); //new String()
//        System.out.println(hello == ("Hel" + lo).intern());
//
//        String str2 = hello.toUpperCase();
//        String str3 = hello.toLowerCase();
//
//        System.out.println(str2 + str3);
//        System.out.println(str2.concat(str3));
//
//        System.out.println(str2.contains("ell"));

        //1. Объявить строки,
        // перевести их в верхний,
        // нижний регист,
        // проверить вхождение подстроки
        // получить подстроку из другой строки

        int[] demo = new int[]{100, 1000, 10000, 100000};

        DateFormat dateInstance = DateFormat.getDateInstance(DateFormat.DATE_FIELD, Locale.ENGLISH);
        NumberFormat instance = NumberFormat.getInstance(Locale.getDefault());

        for (int i : demo) {
            System.out.println(instance.format(i));
        }

        System.out.println(dateInstance.format(new Date()));

//        System.out.println(String.format("%-3.3f", 1234F)); //   1234.000
//        System.out.println(String.format("Information with %s %s", "some string", "some string"));
//
//        System.out.println(str2.concat(str3));

//        User user = new User();
//        System.out.println(user);
//
//        user.setGender(Gender.MALE);
//        System.out.println(user);
//
//        System.out.println(user.getGender().getGenderInfo());
//
//        for (Gender value : Gender.values()) {
//            System.out.println(value + " " + value.ordinal() + " " + value.name() + " " + value.getGenderInfo());
//        }

//
//        long a = 10_000_000_000L;
//        int x;
//        x = (int) a;
//        System.out.println("x - " + x);
//        byte b5 = 50;
//        int b4 = (int) b5*2*2_000_000_000; // error
//        System.out.println(b4);
//
//        //byte b4 = (byte) (b5 * 2);
//        byte b1 = 50, b2 = 20, b3 = 127;
//        int x2 = b1 * b2 * b3;
//        System.out.println("x2 - " + x2);
//        double d = 12.34;
//        int x3;
//        x3 = (int) d;
//        System.out.println("x3 - " + x3);
    }
}
