package com.htp.polymorph.enums;

public enum Gender {
    MALE("Male", "Male Gender"),
    FEMALE("Female", "Female Gender"),
    NOT_SELECTED("Not Selected", "Not Selected");

    public final String genderInfo;
    public final String description;

    Gender(String genderInfo, String description) {
        this.genderInfo = genderInfo;
        this.description = description;
    }

    public String getGenderInfo() {
        return genderInfo;
    }

    public String getDescription() {
        return description;
    }
}