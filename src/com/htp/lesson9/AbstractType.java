package com.htp.lesson9;

import com.htp.polymorph.A;
import com.htp.polymorph.B;
import com.htp.production.domain.Car;
import com.htp.production.domain.Truck;

import java.sql.SQLOutput;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class AbstractType<K extends A, V extends A> {
    private K key;
    private V value;

    public AbstractType() {
    }

    public AbstractType(K key, V value) {
        this.key = key;
        this.value = value;
    }



    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public void someMethod(AbstractType<K, V> something) {

    }

    public void someMethod2(AbstractType<K, ? extends B> something){

    }

    public void someMethod3(AbstractType<?, ?> something){

    }

    public void someMethod4(AbstractType<? super B, V> something){

    }

    public static void main(String[] args) {
        AbstractType<A, A> objectAbstractType = new AbstractType<>(); //diamond operator
        objectAbstractType.setKey(new A(1));
        objectAbstractType.setValue(new A(1));

        AbstractType<B, A> aIntegerAbstractType = new AbstractType<>();
        AbstractType<A, A> aIntegerAbstractType1 = new AbstractType<>();

        objectAbstractType.someMethod3(aIntegerAbstractType1);

//        List list = new LinkedList();
//        list.add("First");
//        list.add(10);
//        List<String> list2 = list;
//        for(Iterator<String> itemItr = list2.iterator(); itemItr.hasNext();)
//            System.out.println(itemItr.next());
//
//        List<String> list = new LinkedList<String>();
//        list.add("First");
//        list.add(new Integer(10));
//        List list2 = list;
//        for(Iterator<String> itemItr = list2.iterator(); itemItr.hasNext();)
//            System.out.println(itemItr.next());

        new CarUtil().printPrice(new Car());
        new CarUtil().printPrice(new Truck());
    }
}
