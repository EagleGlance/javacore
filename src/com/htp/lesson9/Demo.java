package com.htp.lesson9;

public class Demo extends Object {
    {
        System.out.print("1");
    }

    static {
        System.out.print("2");
    }

    Demo() {
        System.out.print("3");
    }

    public static void main(String[] args) {
        System.out.print("4");
//        method();

//        int a1[] = {};
//        int a2[] = new int[]{1,2,3};
//        int a3[] = new int[](1,2,3);
//        int a4[] = new int[3];
//        int a5[] = new int[3]{1,2,3};

//        int i = 5, j = 6;

//        if (i<j) { System.out.print("-1-"); }
//        if (i<j) then  System.out.print("-2-");
//        if i<j { System.out.print("-3-"); }
//        if [i<j] System.out.print("-4-");
//        if (i<j) System.out.print("-5-");
//        if {i<j} then System.out.print("-6-");.


    }

    public static final void method() {
    }

    public void method2(){
        method();
    }
}
