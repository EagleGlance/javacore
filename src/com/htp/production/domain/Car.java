package com.htp.production.domain;

import java.util.Comparator;
import java.util.Objects;

public class Car extends Mechanism implements Comparator<Car> {
    public static int counter3;
    public static int counter4;

    public int counter7;
    public int counter8;

    private Door door;
    private Engine engine;



    static {
        counter3 = 0;
        System.out.println("Static block counter3");
    }

    static {
        counter4 = 0;
        System.out.println("Static block counter4");
    }

    {
        counter7 = 100;
        System.out.println("Logic block counter7");
    }

    {
        counter8 = 100;
        System.out.println("Logic block counter8");
    }

    public Car() {
        super(1 , 2);
        System.out.println("In Car Default Constr");
    }

    public Car(Door door, Engine engine) {
        super(1 , 2);
        this.door = door;
        this.engine = engine;

        System.out.println("In Car Constr With Params");
    }

    public Door getDoor() {
        return door;
    }

    public void setDoor(Door door) {
        this.door = door;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public int compare(Car o1, Car o2) {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(door, car.door) &&
                Objects.equals(engine, car.engine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(door, engine);
    }

    @Override
    public String toString() {
        return "Car{" +
                "door=" + door +
                ", engine=" + engine +
                '}';
    }
}
