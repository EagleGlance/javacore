package com.htp.production.domain;

public class Mechanism {
    public int someInt;
    protected int protectedInt;

    public static int counter1;
    public static int counter2;

    static {
        counter1 = 0;
        System.out.println("Static block counter1");
    }

    static {
        counter2 = 0;
        System.out.println("Static block counter2");
    }

    {
        someInt = 100;
        System.out.println("Logic block someInt");
    }

    {
        protectedInt = 100;
        System.out.println("Logic block protectedInt");
    }

//    public Mechanism() {
//        System.out.println("In Mechanism Default Constr");
//    }

    public Mechanism(int someInt, int protectedInt) {
        this.someInt = someInt;
        this.protectedInt = protectedInt;
        System.out.println("In Mechanism Constr With Params");
    }

    public void someMEthod(){

    }

    protected void someProtectedMethdo(){}
}