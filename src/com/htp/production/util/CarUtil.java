package com.htp.production.util;

import com.htp.production.domain.Car;

public class CarUtil {
    public static double carPrice(Car car) {
        return car.getDoor().getPrice() + car.getEngine().getPrice();
    }
}
