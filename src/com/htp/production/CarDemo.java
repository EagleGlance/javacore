package com.htp.production;

import com.htp.production.domain.Car;
import com.htp.production.domain.Door;
import com.htp.production.domain.Engine;

import static com.htp.production.util.CarUtil.carPrice;

public class CarDemo {
    public static void main(String[] args) {
        Door door1 = new Door(1, 100, "Door 1");
        Door door2 = new Door(2, 200, "Door 2");

        Engine engine1 = new Engine(1, 1000, "Engine 1");
        Engine engine2 = new Engine(2, 2000, "Engine 2");

        //System.out.println(Mechanism.counter1);
        System.out.println(Car.counter4);

        Car car1 = new Car(door1, engine1);
        Car car2 = new Car(door2, engine2);

        System.out.println(carPrice(car1));
        System.out.println(carPrice(car2));
    }
}
