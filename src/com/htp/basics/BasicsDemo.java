package com.htp.basics;

import java.util.ArrayList;
import java.util.Iterator;

public class BasicsDemo {
    public static void main(String[] args) {

        /*If Statements*/
        if (1 > 5/*условие*/) {
            /*1*/
            System.out.println("True expression");
        } else {
            /*2*/
            System.out.println("False expression");
        }

        System.out.println(1 > 5 ? "True ternar expression" : "False ternar expression");

        /*Loops*/
        /*for with if expression*/
        /**/
        int startValue = 7;
        for (int i = 0; i < 10; i++) {
            System.out.println(startValue + "" + "*" + i + " = " + startValue*i);
            //System.out.println(i);
        }

        int min = 10;
        int max = 35;
        int step = 5;
        for (int i = min; i < max; i += step) {
            System.out.println(i);
        }


        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.println(i + "*" + j + " = " + i*j);
            }
        }

        /*while*/

        int whileTemp = 1;
        while (whileTemp % 17 != 0) {
            //тело цикла
            ++whileTemp;
            System.out.println("Value: " + whileTemp);
        }

        int doWhileExample = 0;
        do {
            System.out.println("qweqweqweewqeqwweq");
        } while (doWhileExample % 2 != 0);

        /*foreach*/
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("H");
        objects.add("D");
        objects.add("Q");
        objects.add("Q");
        objects.add("Q");
        objects.add("Q");
        for (Object object : objects) { //iterator
            System.out.println(object);
        }

        System.out.println("Example: " + " = " + 10);

        String switchValue = "SAT";
        int value = 1;
        double qwe = 2.0;
        switch(value) {
            case 1:
                System.out.println("Yahoo Monday!");
                break;
            case 2:
                System.out.println("Wednesday");
                break;
        }

        String[] stringArray = new String[100];
        String[] strings = new String[1000];

        /*Sum all elements of array*/
        int[] testArray = new int[20];
        for (int i = 0; i < testArray.length; i++) {
            testArray[i] = i;
        }
        //testArray.length - array length variable
        int sum = 0;
        for (int i : testArray) {
            sum += i;
        }

        System.out.println("Result sum is: " + sum);



        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = "Some string " + i;
        }
//        ArrayList<Object> objects1 = new ArrayList<>();
//        Iterator<Object> iterator = objects1.iterator();
//        while(iterator.hasNext()){
//            System.out.println(iterator.next());
//        }
        for (String s : stringArray) {
            System.out.println(s);
        }
    }
}
