import com.htp.polymorph.A;
import com.htp.production.domain.Engine;
import com.sun.org.glassfish.external.statistics.annotations.Reset;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.annotation.Repeatable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FirstTest {

    public Engine firstTestEngine;
    public Engine secondTestEngine;
    public Engine thirdTestEngine;

    @Before
    public void generate() {
        firstTestEngine = new Engine(1, 100, "F12");
        secondTestEngine = new Engine(1, 100, "F12");
        thirdTestEngine = new Engine(1, 100, "F12");
    }

    @Test
    public void someTest() {

        A a = new A();
        A b = null;

        Assert.assertNotNull(a);
        Assert.assertNotNull(b);
    }

    @Test
    public void someTestNew() {
        List<Engine> engines = Arrays.asList(firstTestEngine, secondTestEngine, thirdTestEngine);
        engines = engines.stream().filter(e -> e.getPrice() > 100).collect(Collectors.toList());
        Assert.assertEquals(engines.size(), 0);
    }

    @Test
    public void someTestUnique() {
        Set<Engine> engines = new HashSet<>(Arrays.asList(firstTestEngine, secondTestEngine, thirdTestEngine));
        Assert.assertEquals( 3, engines.size());
    }

    @Test(expected = Exception.class)
    public void someExceptionTest() throws Exception {
        firstTestEngine.throwException();
    }

    @Test(timeout = 100)
    public void somePerformanceTest() throws Exception {
        firstTestEngine.perfTest();
    }
}
